package pta08;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


class Student1 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private int age;
	private double grade;
	
	public Student1(){
		
	}
	public Student1(int id, String name, int age, double grade) {
		this.id = id;
		this.setName(name);
		this.setAge(age);
		this.setGrade(grade);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if (name.length()>10){
			throw new IllegalArgumentException("name's length should <=10 "+name.length());
		}
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		if (age<=0){
			throw new IllegalArgumentException("age should >0 "+age);
		}
		this.age = age;
	}
	public double getGrade() {
		return grade;
	}
	@Override
	public String toString() {
		return "Student1 [id=" + id + ", name=" + name + ", age=" + age + ", grade=" + grade + "]";
	}
	public void setGrade(double grade) {
		if (grade<0 || grade >100){
			throw new IllegalArgumentException("grade should be in [0,100] "+grade);
		}
		this.grade = grade;
	}
}
public class test4 {

	public static void main(String[] args) {
		Student1[] students=new Student1[3];
		students[0]=new Student1(1,"张三",19,65);
		students[1]=new Student1(2,"李四",19,75);
		students[2]=new Student1(3,"王五",20,85);
		try {//201521123013
			ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("student1.txt"));
			out.writeObject(students);
			out.close();
			
			ObjectInputStream in=new ObjectInputStream(new FileInputStream("student1.txt"));
			Student1[] newStudent =(Student1[]) in.readObject();
			in.close();
			
			for (int i = 0; i < newStudent.length; i++) {
				System.out.println(newStudent[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

//public class test4 {
//	public static void main (String[] args)throws IOException {
//		Student[] students=new Student[3];
//		students[0]=new Student(1,"张三",19,65);
//		students[1]=new Student(2,"李四",19,75);
//		students[2]=new Student(3,"王五",20,85);
//		DataOutputStream dos=null;
//		try{
//				dos=new DataOutputStream(new BufferedOutputStream(new FileOutputStream("student2.data")));
//				writeData(students, dos);  
//		}finally {
//			dos.close();
//		}
//		DataInputStream dis= new DataInputStream(new BufferedInputStream(new FileInputStream("student2.data")));
//		try {
//			Student[] newStudent=readData(dis);
//			System.out.println(newStudent.length);
//			for (Student student : newStudent) {
//				System.out.println(student);
//			}
//		} finally {
//			dis.close();
//		}	
//	}//201521123013
//	private static void writeData(Student[] s, DataOutput dos)throws IOException
//	   {
//		  dos.writeInt(s.length);
//	      for (int i = 0; i < s.length; i++) {
//			dos.writeInt(s[i].getId());
//			dos.writeUTF(s[i].getName());
//			dos.writeInt(s[i].getAge());
//			dos.writeDouble(s[i].getGrade());
//		 }	       
//	   }
//	   private static Student[] readData(DataInput dis) throws IOException
//	   {
//		  int n=dis.readInt();
//	      Student[] students = new Student[n];
//	      try {
//			for (int i = 0; i < students.length; i++) {
//				students[i]=new Student();
//				students[i].setId(dis.readInt());
//				students[i].setName(dis.readUTF());
//				students[i].setAge(dis.readInt());
//				students[i].setGrade(dis.readDouble());
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	      return students;
//	   }
//}
