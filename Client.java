package pta09;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * This program makes a socket connection to the atomic clock in Boulder, Colorado, and prints the
 * time that the server sends.
 * @version 1.20 2004-08-03
 * @author Cay Horstmann
 */
public class Client
{
   public static void main(String[] args)
   {
      try
      {
         Socket s = new Socket("127.0.0.1", 8189);
         try
         {//201521123013
            InputStream inStream = s.getInputStream();
            Scanner in = new Scanner(inStream);
            Runnable test=new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					 PrintWriter out;
					try {
						out = new PrintWriter(s.getOutputStream());
						 Scanner sc=new Scanner(System.in);
						 while(true){
				            String line1=sc.next(); 
				            out.println(line1);
				            out.flush();
						 }
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			           
				}
			};
			Thread th=new Thread(test);
			th.start();
          /*  PrintWriter out= new PrintWriter(s.getOutputStream());
            Scanner sc=new Scanner(System.in);
            String line1=sc.nextLine(); 
            out.println(line1);
            out.flush();*/
            while (in.hasNext())
            {
               String line = in.nextLine();
               System.out.println(line);
            }
         }
         finally
         {
            s.close();
         }
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }
}



///**
// * This program makes a socket connection to the atomic clock in Boulder, Colorado, and prints the
// * time that the server sends.
// * @version 1.20 2004-08-03
// * @author Cay Horstmann
// */
//public class Client
//{
//   public static void main(String[] args)
//   {
//      try
//      {//201521123013
//         Socket s = new Socket("cec.jmu.edu.cn", 80);
//         try
//         {
//            InputStream inStream = s.getInputStream();
//            Scanner in = new Scanner(inStream);
//          
//            while (in.hasNextLine())
//            {
//               String line = in.nextLine();
//               System.out.println(line);
//            }
//         }
//         finally
//         {
//            s.close();
//         }
//      }
//      catch (IOException e)
//      {
//         e.printStackTrace();
//      }
//   }
//}
//
