package third;

import java.util.Scanner;

class Person {
	private String name;
	private boolean gender;
	private int age;
	private int id;

	public Person() {
		System.out.println("This is constructor");
		System.out.printf("%s,%d,%s,%d\n", name, age, gender, id);
	}

	public Person(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}

public class Main {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int n = Integer.parseInt(in.nextLine());

		Person[] persons = new Person[n];
		for (int i = 0; i < persons.length; i++) {
			Person person = new Person(in.next(), in.nextInt(), in.nextBoolean());
			persons[i] = person;
		}
		for (int j =  persons.length-1; j >=0; j--) {
			System.out.println(persons[j].toString());
		}
		Person p=new Person();
		System.out.println(p.toString());
	}

}