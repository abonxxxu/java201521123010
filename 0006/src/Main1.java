import java.util.Arrays;
import java.util.Scanner;

public class Main1 {
	public static class Main {

		@SuppressWarnings("resource")
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			Scanner in=new Scanner(System.in);
			int n=in.nextInt();
			Car[] c=new Car[n];
			for(int i=0;i<c.length;i++){
			if(c[i]!=c[i-1]){
					i++;
			}
			else continue;
			}
			System.out.println(Arrays.toString(Car.class.getConstructors()));
	  
		}
		class Brand{
			private String name;
			public String toString() {
				return "Brand [name=" + name + "]";
				
			}
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
	}
		class Car{
			private String id;
			private Brand brand;
		
		public Car(String id ,Brand brand){
			this.id = id;
			this.brand = brand;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Brand getBrand() {
			return brand;
		}

		public void setBrand(Brand brand) {
			this.brand = brand;
		}

		@Override
		public String toString() {
			return "Car [id=" + id + ", brand=" + brand + "]";
		}
		}
	}
}
