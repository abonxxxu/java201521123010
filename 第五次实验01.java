package abon;

import java.util.Arrays;
import java.util.Scanner;

public abstract class Person implements Comparable<Person>{
     
		private String name;
		private int age;
		
		public Person(String name , int age){
			this.name = name;
			this.age = age;
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		@Override
		public String toString() {
			return "name-" + name + "age" + age ;
		}
		public int compareTo(Person o) {
			// TODO Auto-generated method stub
			int x = this.name.compareTo(o.name);
			if(x!=0) return x;
			return this.getAge()-o.getAge();
		}

	public void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc =new Scanner(System.in);
		int n = sc.nextInt();
		Person[] ps = new Person[n];
		for(int i=0;i<n;i++){
			ps[i]=new Person(sc.next(),sc.nextInt());
		}
		Arrays.sort(ps);
		for(int i=0;i<n;i++)
		{
			System.out.println(ps[i].toString());
			
		}
		System.out.println(Arrays.toString(Person.class.getInterfaces()));
	}

}


/*
    首先输入n
    输入n行name age，并创建n个对象放入数组
    对数组进行排序后输出。
    最后一行使用System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));
输出PersonSortable所实现的所有接口

*/