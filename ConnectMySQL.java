package pta10;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectMySQL {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException {
		
		//String URL = "jdbc:mysql://localhost:3306/test";
		String URL = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=false";
		String driverName = "com.mysql.jdbc.Driver";
		String sql =   "select * from student";
		String userName = "root";//root
		String password = "87552624xy";//123456
		Connection conn = null;
		
		Class.forName(driverName);
		
	
		try {
			conn = DriverManager.getConnection(URL,userName,password);
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			// id | stuno     | name | age | birthdate  
			while(resultSet.next()){
				int id = resultSet.getInt("id");
				String stuno = resultSet.getString("stuno");
				Date date = resultSet.getDate("birthdate");
				System.out.println("id="+id+" stuno="+stuno+" birthdate="+date);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(conn!=null)
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			conn = null;
		}
	}
}
