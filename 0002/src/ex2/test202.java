package ex2;

import java.util.Scanner;
public class test202 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		StringBuilder stringBuilder = new StringBuilder();
		while (scanner.hasNextInt()) {
			int n = scanner.nextInt();
			int start = scanner.nextInt();
			int end = scanner.nextInt();
			for (int i = 0; i < n; i++) {
				stringBuilder.append(i);
			}
			System.out.println(stringBuilder.substring(start, end));
		}
		scanner.close();
	}
}
