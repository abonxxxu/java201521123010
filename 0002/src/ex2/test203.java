package ex2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
public class test203 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		String[] strings = new String[n];
		scanner.nextLine();
		for (int i = 0; i < n; i++) {
			strings[i] = scanner.nextLine();
		}
		Comparator<String> comparator = new MyComparator();
		Arrays.sort(strings, comparator);
		while (scanner.hasNextLine()) {
			String order = scanner.nextLine();
			if (order.compareTo("sort1") == 0) {
				for (int i = 0; i < n; i++) {
					System.out.println(strings[i].substring(6, 10) + "-" + strings[i].substring(10, 12) + "-" + strings[i].substring(12, 14));
				}
			}
			else if (order.compareTo("sort2") == 0) {
				for (int i = 0; i < n; i++) {
					System.out.println(strings[i]);
				}
			}
			else {
				System.out.println("exit");
				scanner.close();
				System.exit(0);
			}
		}
	}
}
class MyComparator implements Comparator<String> {
	public int compare(String s1, String s2) {
		return s1.substring(6, 14).compareTo(s2.substring(6, 14));
	}
}