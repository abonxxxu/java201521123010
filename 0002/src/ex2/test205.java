package ex2;

import java.math.BigDecimal;
import java.util.Scanner;
public class test205 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNextLine()) {
			String s = scanner.nextLine();
			String t = scanner.nextLine();
			BigDecimal xBigDecimal = new BigDecimal(s);
			BigDecimal yBigDecimal = new BigDecimal(t);
			System.out.println(xBigDecimal.add(yBigDecimal));
			System.out.println(xBigDecimal.multiply(yBigDecimal));
		}
		scanner.close();
	}
}