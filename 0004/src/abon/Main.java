package abon;


import java.util.Scanner;
class Person{
	private String name;
	private boolean gender;
	private int age;
	private int id;
	static int a=0;
	static{
		System.out.println("This is static initialization block");
	}
	{
		this.id=a++;
		System.out.printf("This is initialization block, id is %d%n",id);
	}
	public Person(){
		System.out.println("This is constructor");
		System.out.println(name+','+age+','+gender+','+id);
	}
	public Person(String name,int age,boolean gender){
		this.name=name;
		this.age=age;
		this.gender=gender;
	}
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}
}
public class Main {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int n=in.nextInt();
		Person[] p=new Person[n];
		for(int i=0;i<p.length;i++){
			p[i]= new Person(in.next(),in.nextInt(),in.nextBoolean());
		}
		for(int j=p.length-1;j>=0;j--){
			System.out.println(p[j]);
		}
		System.out.println(new Person());
	}
}
