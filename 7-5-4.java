class Account{
	private int balance;
	
	public Account(int balance) {
		super();
		this.balance = balance;
	}
	public int getBalance() {
		return balance;
	}
	public synchronized void deposit(int money){
		
		this.balance=getBalance()+money;
		
	}
	public synchronized void withdraw(int money){
		this.balance=getBalance()-money;

	}
}