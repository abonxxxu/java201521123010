package pta08;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class test5 {

	public static void main(String[] args) throws IOException{
		List<Student> list=new ArrayList<Student>();
		BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream("d:/Students.txt"), "UTF-8"));
		String line=null;
		while((line=br.readLine())!=null){
			String[] student=line.split(" ");
			int id=Integer.parseInt(student[0]);
			String name=student[1];
			int age=Integer.parseInt(student[2]);
			double grade=Double.parseDouble(student[3]);
			list.add(new Student(id,name,
					age,grade));
		}
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
		br.close();
	}
}
