package pta10;

import java.util.Date;

public class Student {
		private String stuno;
		private String name;
		private int age;
		private Date  birthdate;
		public Student(String stuno, String name, int age, Date birthdate) {
			super();
			this.stuno = stuno;
			this.name = name;
			this.age = age;
			this.birthdate = birthdate;
		}
		public Student() {
			// TODO Auto-generated constructor stub
		}
		
		public void setStuno(String stuno) {
			this.stuno = stuno;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public void setBirthdate(Date birthdate) {
			this.birthdate = birthdate;
		}
		public String getStuno() {
			return stuno;
		}
		public String getName() {
			return name;
		}
		public int getAge() {
			return age;
		}
		public Date getBirthdate() {
			return birthdate;
		}
		@Override
		public String toString() {
			return "Student [stuno=" + stuno + ", name=" + name + ", age=" + age + ", birthdate=" + birthdate + "]";
		}
		
}
