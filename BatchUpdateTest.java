﻿package pta10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

public class BatchUpdateTest {
	String url = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=false";
	String driverName = "com.mysql.jdbc.Driver";	 String userName = "root";//root	
	String password = "87552624xy";
    int num = 1000;
    String strSql = "delete from student(stuno,name,age,birthdate) values(?,?,?,?)";

    @Test
    public void slowTest() throws ClassNotFoundException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = DriverManager.getConnection(url, userName, password);
            con.setAutoCommit(false);
            // 根据参数的插入数据
            pstmt = con.prepareStatement(strSql);
            for (int i = 0; i < num; i++) {
            	pstmt.setString(1, "2015022");
                pstmt.setString(2, "郭雅清");
                pstmt.setInt(3, 11);
                pstmt.setDate(4, java.sql.Date.valueOf("2000-09-02"));
                pstmt.executeUpdate();
            }
            con.commit();
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        } finally {
            if (rs != null)
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (pstmt != null){
                try {
                    pstmt.close();// 关闭语句*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();// 关闭连接
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println("slow test end");
        }
    }

    @Test
    public void batchTest() throws ClassNotFoundException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = DriverManager.getConnection(url, userName, password);
            con.setAutoCommit(false);
            pstmt = con.prepareStatement(strSql);
            for (int i = 0; i < num; i++) {
                pstmt.setString(1, "2015022");
                pstmt.setString(2, "郭雅清");
                pstmt.setInt(3, 11);
                pstmt.setDate(4, java.sql.Date.valueOf("2000-09-02"));
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            con.commit();

        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        } finally {
            if (rs != null)
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (pstmt != null)
                try {
                    pstmt.close();// 关闭语句*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (con != null) {
                try {
                    con.close();// 关闭连接
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println("batch test end");
        }
    }

}
