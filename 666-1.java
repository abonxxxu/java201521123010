public interface MyAbstractList<E> {
    
    boolean add(E e);
    E get(Object object);
    
}

//输入输出的接口，因为后面要用图形界面，到时只要new的时候换个别的类就好了吧
public interface InputAndOutput {
    String nextLine();
    int nextInt();
    void print(String string);
}


//Menu类 显示主菜单，除了退出每次都要能回来
public void showMainMenu() {
    inputAndOutput.print("\t\t1.浏览商品");
    inputAndOutput.print("\t\t2.搜索商品");
    inputAndOutput.print("\t\t3.我的信息");
    inputAndOutput.print("\t\t4.查看购物车");
    inputAndOutput.print("\t\t5.退出");
    inputAndOutput.print("请输入你的选择：");
    switch (inputAndOutput.nextInt()) {
    case 1:
        surfAllGoods();
        break;
        
        
    case 2:
        startSearch();
        
        break;
        
    case 3:
        showMyself(user);
        break;
        
    case 4:
        showShoppingCart(user.getShoppingCart());
        break;
    
    case 5:
        
        System.exit(0);
        break;

    default:
        break;
    }
    showMainMenu();
}


//Menu类 就是每次都要问你要不要买的烦人的方法
private void purchaseOrReturn(AllGoods allGoods) {
    // TODO Auto-generated method stub
    inputAndOutput.print("输入商品索引查看商品或者输入0返回");
    int i = inputAndOutput.nextInt();
    switch (i) {
    case 0:
        break;

    default:
        Goods goods = allGoods.getGoodsList().get(i - 1).showDetails();
        inputAndOutput.print("输入1加入购物车，输入0返回主界面");
        switch (inputAndOutput.nextInt()) {
        case 1:
            inputAndOutput.print("请输入购买商品的个数：");
            user.getShoppingCart().add(user.getShoppingCart().new Item(goods, inputAndOutput.nextInt()));
            break;

        default:
            
            break;
        }
        break;
    }
}

//Search类 核心方法，其实也没写什么东西，按照如滨的意思，后面还要再改
void search() {
    String[] keywords = inputKeyword();
    
    for (Goods goods : allGoods.getGoodsList()) {
        boolean flag = true;
        for (String string : keywords) {
            if (!goods.getName().contains(string)) {
                flag = false;
                break;
            }
        }
        if (flag) {
            searchList.add(goods);
            showGoods(goods);               
        }
    }
    
    if (searchList.isEmpty()) {
        System.out.println(keywords[0]);
        showNoSearched();
    }
}
    
//ShoppingCart类 的方法们
public boolean delete(int i) {
    Item deleteItem = (Item) get(i);
    if (deleteItem != null) {
        totalPrice -= deleteItem.getNum() * deleteItem.getGoods().getPrice();
        items.remove(deleteItem);
        return true;
    }
    return false;
    
}

public boolean clear() {
    if (items.isEmpty()) {
        return false;
    } else {
        
        displayOrder();
        
        items.clear();
        totalPrice = 0;
        
        return true;
    }
}

public void displayAll() {
    if (items.isEmpty()) {
        inputAndOutput.print("购物车还是空的哦！");
    } else {
        for (int i = 0; i < items.size(); i++) {
            inputAndOutput.print(i + 1 + ". " + items.get(i).toString());
        }
        inputAndOutput.print("总价：" + totalPrice + "元");
    }
    
}

private void displayOrder() {
    // TODO Auto-generated method stub
    for (Item item : items) {
        inputAndOutput.print
        ("确认购买" + item.goods + "共， " + item.num + "件");
    }
    
    inputAndOutput.print("总价为" + totalPrice);
    
}

//User类 下面就是一些getter/setter
public class User {
    private String userName;
    private String password;
    private String address;
    private ShoppingCart shoppingCart;
    
    public User(String userName, String password, String address) {
        super();
        this.userName = userName;
        this.password = password;
        this.address = address;
        this.shoppingCart = new ShoppingCart();
    }
    ...
}


//作为一个抽象类Goods，也要有些抽象方法
public abstract class Goods {
    private String name;
    private double price;
    
    //略去了很多getter/setter
    
    public abstract Goods show();
    public abstract Goods showDetails();
    
}


//这次是做控制台，so
package shopping;

import java.util.Scanner;

public class Console implements InputAndOutput {

    @Override
    public String nextLine() {
        // TODO Auto-generated method stub
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        return string;
    }

    @Override
    public int nextInt() {
        // TODO Auto-generated method stub
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        return x;
    }

    @Override
    public void print(String string) {
        // TODO Auto-generated method stub
        System.out.println(string);
        
    }

}

//图书类其实没啥，就是衣服类会比较复杂，首先是ClothesType类
enum Color{
    黑色, 白色, 蓝色 
}

enum Size {
    S, M, L, XL, XLL
}

public class ClothesType extends Goods {
    private Size size;
    private Color color;
    private InputAndOutput inputAndOutput = new Console();
    
    public ClothesType(String name, double price) {
        super(name, price);
    }
    
    @Override
    public String toString() {
        return "服装类    " + super.toString();
    }


    public Goods show() {
        inputAndOutput.print(toString());
        return this;
    }
    
    
    public Clothes confirm() {
        inputAndOutput.print("颜色:");
        int id = 0;
        for (Color color : Color.values()) {
            inputAndOutput.print(++id + "." + color);
        }
        inputAndOutput.print("请选择：");
        int c = inputAndOutput.nextInt();
        
        
        inputAndOutput.print("大小:");
        id = 0;
        for (Size size : Size.values()) {
            inputAndOutput.print(++id + "." + size);
        }
        inputAndOutput.print("请选择：");
        int s = inputAndOutput.nextInt();
        
        
        return new Clothes(super.getName(), super.getPrice(), Color.values()[c - 1], Size.values()[s - 1]);
        
    }

    @Override
    public Goods showDetails() {
        // TODO Auto-generated method stub
        inputAndOutput.print(toString());
        return confirm();
    }
}

//但是衣服还有具体的颜色和大小，so这边又加了个类，就是加了两个设置的方法
public class Clothes extends ClothesType {

    public Clothes(String name, double price, Color color, Size size) {
        super(name, price);
        setColor(color);
        setSize(size);
    }
    
    
    @Override
    public String toString() {
        return "Clothes [Size" + getSize() + ", Color()=" + getColor()
                        + ", " + super.toString() + "]";
    }

}
