package ex2;

import java.util.Arrays;
import java.util.Scanner;
public class test201 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int [] nums = null;
		while (scanner.hasNextLine()) {
			String string = scanner.nextLine();
			if (string.equals("fib")) {
				int n = scanner.nextInt();
				if (n == 1) {
					System.out.println(1);
				} else {
					int[] fib = new int[n];
					fib[0] = fib[1] = 1;
					System.out.print("1 1");
					for (int i = 2; i < n; i++) {
						fib[i] = fib[i - 1] + fib[i - 2];
						System.out.print(" " + fib[i]);
					}
					System.out.println();
				}
				scanner.nextLine();
			} else if (string.equals("sort")) {
				String[] strings = scanner.nextLine().split(" ");
				nums = new int[strings.length];
				for (int i = 0; i < strings.length; i++) {
					nums[i] = Integer.parseInt(String.valueOf(strings[i]));
				}
				Arrays.sort(nums);
				System.out.println(Arrays.toString(nums));
			} else if (string.equals("search")) {
				int pos = Arrays.binarySearch(nums, scanner.nextInt());
				if (pos < 0) {
					System.out.println(-1);
				} else {
					System.out.println(pos);
				}
				scanner.nextLine();
			} else if (string.equals("getBirthDate")) {
				int n = scanner.nextInt();
				scanner.nextLine();
				String[] strings = new String[n], strings2 = new String[n];
				for (int i = 0; i < n; i++) {
					strings[i] = scanner.nextLine();
					strings2[i] = strings[i].substring(6, 10) + "-" + strings[i].substring(10, 12) + "-" + strings[i].substring(12, 14);
				}
				for (int i = 0; i < n; i++) {
					System.out.println(strings2[i]);
				}
			} else {
				System.out.println("exit");
				break;
			}
		}
		scanner.close();
	}
}